/*global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
/* global L, distance */



/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
var pacienti = [
    {
      "ime": "Janez",
      "priimek": "Novak",
	  "datumRojstva": "1950-08-12",
	  "sistlak": "110",
	  "diatlak": "75"

	  
    },
    {
      "ime": "Marko",
      "priimek": "Skakalec",
	  "datumRojstva": "1999-04-07",
	  "sistlak": "144",
	  "diatlak": "95"
    },
    {
      "ime": "Martin",
      "priimek": "Krpan",
	  "datumRojstva": "1970-11-11",
	  "sistlak": "190",
	  "diatlak": "123"
    }

];

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	var ehrId="";
	var ime = pacienti[stPacienta-1].ime;
	var priimek = pacienti[stPacienta-1].priimek;
	var datumRojstva = pacienti[stPacienta-1].datumRojstva;
	var sist= pacienti[stPacienta-1].sistlak;
	var diat = pacienti[stPacienta-1].diatlak;
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#Sporocilogen").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
				$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		          headers: {
        "Authorization": getAuthorization()
      },
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth:datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		                  headers: {
        "Authorization": getAuthorization()
      },
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		             /*   $("#Sporocilogen").append("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span></br>");*/
		                   /* $("#Sporocilogen").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);*/
		           
		                    
		          var datum = "2018-12-21T11:40Z";
							var Visina ="180";
							var Teza = "80";
							var Temperatura = "36.50";
							var sistolicni = sist;
							var diastolicni = diat;
							var nasicenost = "98";
							var merilec = "Chunga Lunga";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datum,
								    "vital_signs/height_length/any_event/body_height_length": Visina,
								    "vital_signs/body_weight/any_event/body_weight": Teza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": Temperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    		                  headers: {
        "Authorization": getAuthorization()
      },
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#Sporocilogen").append("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' in dodani podatki.</span></br>");
	
								    },
								    error: function(err) {
								    	$("#Sporocilogen").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#Sporocilogen").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});

	}
}
function generirajPac(){
                $("#Sporocilogen").html("");
  generirajPodatke(1);
	
	generirajPodatke(2);
	
	generirajPodatke(3);
}

function kreirajEHRzaBolnika() {

	var ime = $("#Ime").val(); // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek = $("#Priimek").val();
	var sistl=$("#Sistolicni").val();
	var diatl=$("#Diastolicni").val();
	var datumr=$("#DatumRojstva").val();
	var ehrId;
	$("#Ime").val("");
		$("#Priimek").val("");
		$("#DatumRojstva").val("");
		$("#Sistolicni").val("");
		$("#Diastolicni").val("");
	if (!ime || !priimek || !sistl || !diatl || ime.trim().length == 0 || priimek.trim().length == 0 || sistl.trim().length == 0 || diatl.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		          headers: {
        "Authorization": getAuthorization()
      },
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth:datumr,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		                  headers: {
        "Authorization": getAuthorization()
      },
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#EHRid").val(ehrId);
		           
		                    
		          var datum = "2018-12-21T11:40Z";
							var Visina ="180";
							var Teza = "80";
							var Temperatura = "36.50";
							var sistolicni = sistl;
							var diastolicni = diatl;
							var nasicenost = "98";
							var merilec = "Chunga Lunga";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datum,
								    "vital_signs/height_length/any_event/body_height_length": Visina,
								    "vital_signs/body_weight/any_event/body_weight": Teza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": Temperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    		                  headers: {
        "Authorization": getAuthorization()
      },
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' in dodani podatki.</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
}
function preberiEHRodBolnika() {
	var ehrId = $("#EHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#preberiSporocilo").html("<br/><span>Pridobivanje " +
            "podatkov za <b></b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    		var s=res[0].systolic;
    			    		var d=res[0].diastolic;
    				    /*	var results = "<table class='table table-striped" +
                    "table-hover'><tr><th class='text-right'>Sistolični tlak</th>" +
                    "<th class='text-right'>Diastolični tlak</th></tr>";
                   
  				      
    		            results += "<tr><td class='text-right'>" + res[0].systolic + " "+res[0].unit+
                      "</td><td class='text-rght'>" + res[0].diastolic +
                      " " + res[0].unit + "</td></tr>";*/
  				        var results="Podatki od krvnem tlaku za <b>"+party.firstNames+" "+party.lastNames+":</b> </br> <b>Sistolični krvni tlak:</b> "+res[0].systolic+" "+res[0].unit+"</br><b>Diastolični krvni tlak:</b> "+
  				        res[0].diastolic+" "+res[0].unit+"</br>";
  				        if(s<120 && d<80){
  				        	results+="Krvni tlak bolnika spada v kategorijo <b style='color:green'>Normalna raven</b>.</br><b>Priporočilo:</b></br> Bolnik naj nadaljuje z zdravim načinom življenja.";
  				        }
  				        else{
  				        	if((s>=120&&s<140)&&(d>=80&&d<90)){
  				        		results+="Krvni tlak bolnika spada v kategorijo <b style='color: #CCCC00'>Pred-hipertenzija</b>.</br><b>Priporočilo:</b></br> Bolnik ima nekoliko povišan krvni tlak. Bolniku priporočamo telesno aktivnost in zdravo prehrano, o kateri lahko izve več "+
  				        		"spodaj.";
  				        	}
  				        	else{
  				        		if((s>=140&&s<160)&&(d>=90&&d<100)){
  				        			results+="Krvni tlak bolnika spada v kategorijo <b style='color:  orange '>Hipertenzija 1. stopnje</b>.</br><b>Priporočilo:</b></br> Bolnik ima povišan krvni tlak. Bolniku priporočamo zdravila za znižanje krvnega tlaka, obisk zdravnika, telesno aktivnost in zdravo prehrano, "+
  				        			"o kateri lahko izve več spodaj.";
  				        		}
  				        		else{
  				        			if((s>=160&&s<180)&&(d>=100&&d<110)){
  				        			results+="Krvni tlak bolnika spada v kategorijo <b style='color:tomato'>Hipertenzija 2. stopnje</b>.</br><b>Nujno:</b></br> Bolnik ima povišan zelo krvni tlak. Bolnik naj nujno začne jemati zdravila za znižanje krvnega tlaka, obišče naj zdravnika,"+
  				        			" se telesno gibati in ukvarjati z zdravo prehrano, "+
  				        			"o kateri lahko izve več spodaj.";
  				        			}
  				        			else{
  				        				
  				        				results+="Krvni tlak bolnika spada v kategorijo <b style='color: crimson'>Kritična raven</b>.</br> Bolnik <b style='color: crimson'>nujno</b> potrebuje zdravniško pomoč!";
  				        			}
  				        		}
  				        	}
  				        }
  				        
  				        console.log(res);
  				        results += "</table>";
  				        	$("#preberiSporocilo").html("");
  				      		$("#tl").css('display','block');
  				        $("#Sporocilotlak").html(results);
    			    	} else {
    			    		$("#preberiSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
/* global L, distance */


// seznam z markerji na mapi
var markerji = [];

var mapa;




$(document).ready(function() {
	$('#preberiBolnika').change(function() {
		$("#kreirajSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#Ime").val(podatki[0]);
		$("#Priimek").val(podatki[1]);
		$("#DatumRojstva").val(podatki[2]);
		$("#Sistolicni").val(podatki[3]);
		$("#Diastolicni").val(podatki[4]);
	});
	$('#ObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		console.log($(this).val());
		$("#EHRid").val($(this).val());
	});



const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);



  // Objekt oblačka markerja
  var popup = L.popup();
	pridobiPodatke(function (jsonRezultat) {
    izrisRezultatov(jsonRezultat);
  });
  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    var latt=latlng["lat"];
     var lngg=latlng["lng"];
   
 	pridobiPodatke(function (jsonRezultat) {
    var znacilnosti = jsonRezultat.features;
       var latp=znacilnosti[0].geometry.coordinates[0][0][1];
   var lngp=znacilnosti[0].geometry.coordinates[0][0][0];
    var dist=distance(latt, lngg, znacilnosti[0].geometry.coordinates[0][0][1], znacilnosti[0].geometry.coordinates[0][0][0], "K");
mapa.eachLayer(function (layer) {
    mapa.removeLayer(layer);
});
    mapa.addLayer(layer);

 for (var i = 0; i < znacilnosti.length; i++) {

  	if(znacilnosti[i].geometry.type=="Polygon"){
  		if(distance(latt, lngg, znacilnosti[i].geometry.coordinates[0][0][1], znacilnosti[i].geometry.coordinates[0][0][0], "K")<dist){
 				dist=distance(latt, lngg, znacilnosti[i].geometry.coordinates[0][0][1], znacilnosti[i].geometry.coordinates[0][0][0], "K");
 				latp=znacilnosti[i].geometry.coordinates[0][0][1];
 				lngp=znacilnosti[i].geometry.coordinates[0][0][0];
 		}
		if(distance(latt, lngg, znacilnosti[i].geometry.coordinates[0][0][1], znacilnosti[i].geometry.coordinates[0][0][0], "K") <= 2){
					var geojsonFeature = {
    "type": "Feature",
    "properties": znacilnosti[i].properties,
    "geometry": znacilnosti[i].geometry
		};
		L.geoJson(geojsonFeature, {
			color:'green',
			onEachFeature: function(feature,layer){
				layer.on({
					mouseover:function(){
						console.log(feature);
						console.log(feature.geometry.coordinates[0][1][0]);
						var latlng=[feature.geometry.coordinates[0][0][1],feature.geometry.coordinates[0][0][0]];
						var c;
						if(feature.properties["addr:street"]==undefined){
								if(feature.properties["name"]==undefined){
									c=feature.properties["amenity"];
								}
								else
								c=feature.properties["name"]+"</br>"+feature.properties["amenity"];
						}
						else{
							if(feature.properties["name"]==undefined){
									c=feature.properties["addr:city"]+"</br>"+feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"];
								}
								else
							c=feature.properties["name"]+"</br>"+feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"];
						}
						console.log(feature.properties["addr:street"]);
						    popup
    					 .setLatLng(latlng)
    					 .setContent(c)
      				.openOn(mapa);
					}
				})
			}
		}).addTo(mapa);
		}
		else{
			if(distance(latt, lngg, znacilnosti[i].geometry.coordinates[0][0][1], znacilnosti[i].geometry.coordinates[0][0][0], "K")<dist){
 				dist=distance(latt, lngg, znacilnosti[i].geometry.coordinates[0][0][1], znacilnosti[i].geometry.coordinates[0][0][0], "K");
 				latp=znacilnosti[i].geometry.coordinates[0][0][1];
 				lngp=znacilnosti[i].geometry.coordinates[0][0][0];
 		}
		var geojsonFeature = {
    "type": "Feature",
    "properties": znacilnosti[i].properties,
    "geometry": znacilnosti[i].geometry
		};
		L.geoJson(geojsonFeature, {
			color:'blue',
			onEachFeature: function(feature,layer){
				layer.on({
					mouseover:function(){
						console.log(feature);
						console.log(feature.geometry.coordinates[0][1][0]);
						var latlng=[feature.geometry.coordinates[0][0][1],feature.geometry.coordinates[0][0][0]];
						var c;
						if(feature.properties["addr:street"]==undefined){
								if(feature.properties["name"]==undefined){
									c=feature.properties["amenity"];
								}
								else
								c=feature.properties["name"]+"</br>"+feature.properties["amenity"];
						}
						else{
							if(feature.properties["name"]==undefined){
									c=feature.properties["addr:city"]+"</br>"+feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"];
								}
								else
							c=feature.properties["name"]+"</br>"+feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"];
						}
					
						    popup
    					 .setLatLng(latlng)
    					 .setContent(c)
      				.openOn(mapa);
					}
				})
			}
		}).addTo(mapa);
		}
  	}
 		else{
 			if(distance(latt, lngg, znacilnosti[i].geometry.coordinates[1], znacilnosti[i].geometry.coordinates[0], "K")<dist){
 				dist=distance(latt, lngg, znacilnosti[i].geometry.coordinates[1], znacilnosti[i].geometry.coordinates[0], "K");
 				latp=znacilnosti[i].geometry.coordinates[1];
 				lngp=znacilnosti[i].geometry.coordinates[0];
 		}
  			if(znacilnosti[i].geometry.type=="Point"){
  				if(znacilnosti[i].geometry.coordinates[0]!=undefined){
  					  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      'blue' + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  				if(znacilnosti[i].properties.name==undefined){
  					     var opis="Bolnišnica";
  				}
  				else
  				var opis = znacilnosti[i].properties.name;
  		    var lng =znacilnosti[i].geometry.coordinates[0];
    			var lat =  znacilnosti[i].geometry.coordinates[1];
    	var marker = L.marker([lat, lng], {icon: ikona});
					 marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();
			     marker.addTo(mapa);
			     markerji.push(marker);
			     
  				}
  	}
  	}
 }


 
 
  });

  }

  mapa.on('click', obKlikuNaMapo);
  

function pridobiPodatke(callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        // nastavimo ustrezna polja (število najdenih zadetkov)
       
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}
function izrisRezultatov(jsonRezultat) {
  var znacilnosti = jsonRezultat.features;
  

 for (var i = 0; i < znacilnosti.length; i++) {
  	if(znacilnosti[i].geometry.type=="Polygon"){
	
		var geojsonFeature = {
    "type": "Feature",
    "properties": znacilnosti[i].properties,
    "geometry": znacilnosti[i].geometry
		};
		L.geoJson(geojsonFeature, {
			color:'red',
			onEachFeature: function(feature,layer){
				layer.on({
					mouseover:function(){

						var latlng=[feature.geometry.coordinates[0][0][1],feature.geometry.coordinates[0][0][0]];
						var c;
						if(feature.properties["addr:street"]==undefined){
								if(feature.properties["name"]==undefined){
									c=feature.properties["amenity"];
								}
								else
								c=feature.properties["name"]+"</br>"+feature.properties["amenity"];
						}
						else{
													if(feature.properties["name"]==undefined){
									c=feature.properties["addr:city"]+"</br>"+feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"];
								}
								else
							c=feature.properties["name"]+"</br>"+feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"];
						}
					
						    popup
    					 .setLatLng(latlng)
    					 .setContent(c)
      				.openOn(mapa);
					}
				
				})
			}
		}).addTo(mapa);
  	}
  	else{
  			if(znacilnosti[i].geometry.type=="Point"){
  				if(znacilnosti[i].geometry.coordinates[0]!=undefined){
  					  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      'blue' + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  				if(znacilnosti[i].properties.name==undefined){
  					     var opis="Bolnišnica";
  				}
  				else
  				var opis = znacilnosti[i].properties.name;
  		    var lng =znacilnosti[i].geometry.coordinates[0];
    			var lat =  znacilnosti[i].geometry.coordinates[1];
    	var marker = L.marker([lat, lng], {icon: ikona});
					 marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();
			     marker.addTo(mapa);
			     markerji.push(marker);
			     
  				}
  	}
  	}

  }
  
}






});
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


/*global jQuery*/
/*global CanvasJS*/

function izrisipod(){
	var iddrzave=$('#drzava').val();


		$.ajax({
		  url: "https://apps.who.int/gho/athena/api/GHO/BP_06.json?profile=simple&filter=COUNTRY:"+iddrzave,
		  dataType: 'jsonp',
		  success: function (res) {
		  document.getElementById("grafi").style.display = "block";
		   	var leta=[];
				var vr=[];
				var lett=[];
				var vrr=[];
				var st=0;
				var max=0;
				var min=300;
				for(var i=0;i<res.fact.length;i++){
						if(res.fact[i].dim["SEX"]=="Male"){
						if(parseFloat(res.fact[i].Value)>max){
							max=parseFloat(res.fact[i].Value);
						}
						if(parseFloat(res.fact[i].Value)<min){
							min=parseFloat(res.fact[i].Value);
						}
					  	vr[parseInt(res.fact[i].dim["YEAR"])]=parseFloat(res.fact[i].Value);
					  	leta[st]=parseInt(res.fact[i].dim["YEAR"]);
					  	st++;
						}
				}
				leta.sort();
				var l=0;
				for(var i=0;i<leta.length;i++){
					vrr[i]=vr[leta[i]];
				}
				vr=null;
				var dps = [];
				
				for(var i=0;i<leta.length;i++){
				 dps.push({
					      x: leta[i],
					      y: vrr[i]
					    });

				}
				console.log(vrr);
		
			var chart = new CanvasJS.Chart("chartContainer", {
			  title: {
			    text: "Krvni tlak skozi leta za moške"
			  },
			  axisX: {
			  	valueFormatString: "######",
			    title: "Leta"
			  },
			  axisY: {
			  	minimum:parseInt(min)-1,
			  	maximum:parseInt(max)+1,
			    title: "Tlak [mm Hg]"
			  },
			  data: [{
			    type: "line",
			    dataPoints: dps
			  }]
			});
				
			chart.render();	
			
			
			
			
			
			
			
			
			
			
			  	var leta=[];
				var vr=[];
				var lett=[];
				var vrr=[];
				var st=0;
				var max=0;
				var min=300;
				for(var i=0;i<res.fact.length;i++){
						if(res.fact[i].dim["SEX"]=="Female"){
						if(parseFloat(res.fact[i].Value)>max){
							max=parseFloat(res.fact[i].Value);
						}
						if(parseFloat(res.fact[i].Value)<min){
							min=parseFloat(res.fact[i].Value);
						}
					  	vr[parseInt(res.fact[i].dim["YEAR"])]=parseFloat(res.fact[i].Value);
					  	leta[st]=parseInt(res.fact[i].dim["YEAR"]);
					  	st++;
						}
				}
				leta.sort();
				var l=0;
				for(var i=0;i<leta.length;i++){
					vrr[i]=vr[leta[i]];
				}
				vr=null;
				var dps = [];
				
				for(var i=0;i<leta.length;i++){
				 dps.push({
					      x: leta[i],
					      y: vrr[i]
					    });

				}
				console.log(vrr);
							
			var chart = new CanvasJS.Chart("chartContainer2", {
			  title: {
			    text: "Krvni tlak skozi leta za ženske"
			  },
			  axisX: {
			  	valueFormatString: "######",
			    title: "Leta"
			  },
			  axisY: {
			  	minimum:parseInt(min)-1,
			  	maximum:parseInt(max)+1,
			    title: "Tlak [mm Hg]"
			  },
			  data: [{
			    type: "line",
			    dataPoints: dps
			  }]
			});
				
			chart.render();	
				
		  }
		});
}



YUI().use(   "datatable", function (Y) {
	


    var hrana = [
        {  aname: 'Žita',  chars:[ 'Rjav riž','Ajda','Pira','Polnozrnata pšenica','Rž','Oves','Koruza','Ječmen' ] },
        {  aname: 'Sadje', chars:[ 'Granatno jabolko','Paradižnik','Pomaranče','Banane','Marelice','Suhe brusnice' ] },
        {  aname: 'Zelenjava', chars:[ 'Krompir','Sladki krompir','Šparglji','Špinača','Zelje','Kalčki','Rdeča pesa' ] },
        {  aname: 'Beljakovine',  chars:[ 'Jogurt', 'Ribe', 'Posneto mleko', 'Piščanec','Puran' ]} ,
        {  aname: 'Eterična olja',  chars:[ 'Vrtnično','Sivkino','Bergamotovo','Ylang-ylang','Muškatna kadulja','Rožmarinovo','Tamjanovo' ]     }
    ];

    //
    //   Create the "parent" DataTable
    //
    var dt_master = new Y.DataTable({
        columns : [
            { key:'aname',  label:'Kategorija' }
        ],
        data : hrana,
        width:300,
         
    }).render("#mtable");

    //
    // Add a new attribute to track the last TR clicked,
    //   this is used in the details DT formatter below and later
    //   in the row click handler `delegate` for row highlighting
    //
    //  also setup a click listener to update the "selectedRow" attribute on TR
    //  clicks
    //
    dt_master.addAttr("selectedRow", { value: null });

    dt_master.delegate('click', function (e) {
        this.set('selectedRow', e.currentTarget);
     }, '.yui3-datatable-data tr', dt_master);

    //
    //   Create the characters DataTable and render it (it is hidden initially)
    //
    var dt_detail = new Y.DataTable({
        columns : [
            { key:'char_name', label:'Živilo' ,

            }
         ],
        data : [],
           
        strings : {
            emptyMessage : "Niste še izbrali kategorije."
        },       
        width: 1000
    }).render("#dtable");

    //
    //  Setup a listener to the Master "selectedRowChange" event (i.e. after a
    //  row click)
    //
    dt_master.after('selectedRowChange', function (e) {

        var tr = e.newVal,              // the Node for the TR clicked ...
            last_tr = e.prevVal,        //  "   "   "   the last TR clicked ...
            rec = this.getRecord(tr);   // the current Record for the clicked TR

        //
        //  This if-block does double duty,
        //  (a) it tracks the first click to toggle the "details" DIV to visible
        //  (b) it un-hightlights the last TR clicked
        //
        if ( !last_tr ) {
            // first time thru ... display the Detail DT DIV that was hidden
            Y.one("#chars").show();
        } else {
            last_tr.removeClass("myhilite");
        }

        //
        //  After unhighlighting, now highlight the current TR
        //
        tr.addClass("myhilite");


        //
        //  Collect the "chars" member of the parent record into an array of
        //  objects  with property name "aname"
        //
        var detail_data = [];
        if ( rec.get('chars') ) {
            Y.Array.each( rec.get('chars'), function(item){
                detail_data.push( {char_name:item});
            });
        }

        //
        //  Set the "detail_data" to the dt_detail DataTable
        //    also update the heading in "acategory"
        //   ( it automatically refreshes )
        //
        dt_detail.setAttrs({
            data: detail_data,
                
        });
    });

});
